//package com.example.fileprovider;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipException;
//import java.util.zip.ZipFile;
//
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFCellStyle;
//import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
//import org.apache.poi.hssf.usermodel.HSSFDateUtil;
//import org.apache.poi.hssf.usermodel.HSSFPicture;
//import org.apache.poi.hssf.usermodel.HSSFPictureData;
//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFShape;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hwpf.HWPFDocument;
//import org.apache.poi.hwpf.usermodel.CharacterRun;
//import org.apache.poi.hwpf.usermodel.Paragraph;
//import org.apache.poi.hwpf.usermodel.Picture;
//import org.apache.poi.hwpf.usermodel.Range;
//import org.apache.poi.hwpf.usermodel.Table;
//import org.apache.poi.hwpf.usermodel.TableCell;
//import org.apache.poi.hwpf.usermodel.TableIterator;
//import org.apache.poi.hwpf.usermodel.TableRow;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;
//import org.apache.poi.ss.usermodel.WorkbookFactory;
//import org.xmlpull.v1.XmlPullParser;
//import org.xmlpull.v1.XmlPullParserException;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Environment;
//import android.util.Log;
//import android.util.Xml;
//import android.webkit.WebView;
//
//
///**
//* 查看word,excel附件的界面
//* @author chenhao
//* 2013-5-15 上午10:08:12
//*/
///**
//* @author chenhao
//* 2013-5-15 上午10:38:41
//*/
//public class FileReadActivity extends Activity {
//
//	/**
//	 * 显示数据的界面
//	 */
//	private WebView viewWV;
//
//	public static final String FILEPATH = "filePath";
//
//	/**
//	 * 文件路径
//	 */
//	private String filePath;
//
//	/**
//	 * 文档范围
//	 */
//	private Range range = null;
//
//	/**
//	 * 包装文档后的对象
//	 */
//	private HWPFDocument hwpf = null;
//
//	/**
//	 * 图片集合
//	 */
//	private List<Picture> pictures;
//
//	/**
//	 * 表格迭代器
//	 */
//	private TableIterator tableIterator;
//
//	/**
//	 * html保存的路径
//	 */
//	private String htmlPath;
//
//	/**
//	 * html文件
//	 */
//	private File myFile;
//
//	/**
//	 * 写入html的全局文件输出流
//	 */
//	private FileOutputStream output;
//
//	/**
//	 * 文档中当前图片的位置
//	 */
//	private int presentPicture = 0;
//
//	/**
//	 * 图片路径
//	 */
//	private String picturePath;
//
//	/**
//	 * 拼接html语言的字符串
//	 */
//	private StringBuffer lsb = new StringBuffer();
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.common_attachment_view);
//		viewWV = (WebView) this.findViewById(R.id.common_attr_view_wb);
//		viewWV.getSettings().setBuiltInZoomControls(true);
//		viewWV.getSettings().setUseWideViewPort(true);
//		viewWV.getSettings().setSupportZoom(true);
//		initData();
//		String returnPath = read();
//		viewWV.loadUrl(returnPath);
//	}
//
//	/**
//	 * 初始化数据，获取文件路径
//	 * 2013-5-15 上午10:35:51 chenhao添加此方法
//	 */
//	private void initData() {
//		Intent intent = getIntent();
//		filePath = intent.getStringExtra(FILEPATH);
//	}
//
//	/**
//	 * 根据不同的文件实现不同读取
//	 * 2013-5-15 上午10:40:16 chenhao添加此方法
//	 * @param filePath
//	 * @return
//	 */
//	private String read(){
//		String returnPath = "";
//		if(this.filePath.endsWith(".doc")){
//			this.getRange();
//			this.makeFile();
//			this.readDOC();
//
//		}
//
//		if(this.filePath.endsWith(".docx")){
//			this.makeFile();
//			this.readDOCX();
//		}
//
//		if(this.filePath.endsWith(".xls")){
//			try {
//				this.makeFile();
//				this.readXLS();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//
//		if(this.filePath.endsWith(".xlsx")){
//			this.makeFile();
//			this.readXLSX();
//		}
//
//		returnPath = "file:///" + this.htmlPath;
//		return returnPath;
//	}
//
//	/**
//	 * 得到文档的范围并初始化文档中的相关元素
//	 * 2013-5-15 上午10:53:09 chenhao添加此方法
//	 */
//	private void getRange() {
//		FileInputStream in = null;
//		POIFSFileSystem pfs = null;
//
//		try {
//			in = new FileInputStream(filePath);
//			pfs = new POIFSFileSystem(in);
//			hwpf = new HWPFDocument(pfs);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		range = hwpf.getRange();
//
//		hwpf.getPicturesTable();
//
//		pictures = hwpf.getPicturesTable().getAllPictures();
//
//		tableIterator = new TableIterator(range);
//
//	}
//
//	/**
//	 * 创建html文件
//	 * 2013-5-15 上午11:03:43 chenhao添加此方法
//	 */
//	private void makeFile() {
//		String sdStateString = Environment.getExternalStorageState();// 获取外部存储状态
//		if (sdStateString.equals(Environment.MEDIA_MOUNTED)) {// 确认sd卡存在
//			try {
//				File sdFile = Environment
//						.getExternalStorageDirectory();// 获取扩展设备的文件目录
//				String path = sdFile.getAbsolutePath() + File.separator
//						+ "chenhao/attrs";// 得到sd卡(扩展设备)的绝对路径+"/"
//				File dirFile = new File(path);// 获取文件夹地址
//				if (!dirFile.exists()) {// 如果不存在
//					dirFile.mkdir();// 创建目录
//				}
//				File myFile = new File(path + File.separator + "my.html");// 获取my.html的地址
//				if (!myFile.exists()) {// 如果不存在
//					myFile.getParentFile().mkdirs();// 创建文件
//				}
//				this.htmlPath = myFile.getAbsolutePath();// 返回路径
//			} catch (Exception e) {
//			  e.printStackTrace();
//			}
//		}
//	}
//
//	/**
//	 *  读取word中的内容写到sdcard上的.html文件中
//	 */
//	public void readDOC() {
//
//		try {
//			myFile = new File(htmlPath);
//			output = new FileOutputStream(myFile);
//			String head = "<html><meta charset=\"utf-8\"><body>";
//			String tagBegin = "<p>";
//			String tagEnd = "</p>";
//
//			output.write(head.getBytes());
//
//			int numParagraphs = range.numParagraphs();// 得到页面所有的段落数
//
//			for (int i = 0; i < numParagraphs; i++) { // 遍历段落数
//				Paragraph p = range.getParagraph(i); // 得到文档中的每一个段落
//
//				if (p.isInTable()) {
//					int temp = i;
//					if (tableIterator.hasNext()) {
//						String tableBegin = "<table style=\"border-collapse:collapse\" border=1 bordercolor=\"black\">";
//						String tableEnd = "</table>";
//						String rowBegin = "<tr>";
//						String rowEnd = "</tr>";
//						String colBegin = "<td>";
//						String colEnd = "</td>";
//
//						Table table = tableIterator.next();
//
//						output.write(tableBegin.getBytes());
//
//						int rows = table.numRows();
//
//						for (int r = 0; r < rows; r++) {
//							output.write(rowBegin.getBytes());
//							TableRow row = table.getRow(r);
//							int cols = row.numCells();
//							int rowNumParagraphs = row.numParagraphs();
//							int colsNumParagraphs = 0;
//							for (int c = 0; c < cols; c++) {
//								output.write(colBegin.getBytes());
//								TableCell cell = row.getCell(c);
//								int max = temp + cell.numParagraphs();
//								colsNumParagraphs = colsNumParagraphs
//										+ cell.numParagraphs();
//								for (int cp = temp; cp < max; cp++) {
//									Paragraph p1 = range.getParagraph(cp);
//									output.write(tagBegin.getBytes());
//									writeParagraphContent(p1);
//									output.write(tagEnd.getBytes());
//									temp++;
//								}
//								output.write(colEnd.getBytes());
//							}
//							int max1 = temp + rowNumParagraphs;
//							for (int m = temp + colsNumParagraphs; m < max1; m++) {
//								temp++;
//							}
//							output.write(rowEnd.getBytes());
//						}
//						output.write(tableEnd.getBytes());
//					}
//					i = temp;
//				}
//
//				else {
//					output.write(tagBegin.getBytes());
//					writeParagraphContent(p);
//					output.write(tagEnd.getBytes());
//				}
//			}
//			String end = "</body></html>";
//			output.write(end.getBytes());
//			output.close();
//		} catch (Exception e) {
//			System.out.println("readAndWrite Exception");
//		}
//	}
//
//
//	/**
//	 * 读取07及以上版本的word文档
//	 * 2013-5-15 下午2:38:33 chenhao添加此方法
//	 */
//	private void readDOCX() {
//		String river = "";
//		try {
//			this.myFile = new File(this.htmlPath);// new一个File,路径为html文件
//			this.output = new FileOutputStream(this.myFile);// new一个流,目标为html文件
//			String head = "<!DOCTYPE><html><meta charset=\"utf-8\"><body>";// 定义头文件,我在这里加了utf-8,不然会出现乱码
//			String end = "</body></html>";
//			String tagBegin = "<p>";// 段落开始,标记开始?
//			String tagEnd = "</p>";// 段落结束
//			String tableBegin = "<table style=\"border-collapse:collapse\" border=1 bordercolor=\"black\">";
//			String tableEnd = "</table>";
//			String rowBegin = "<tr>";
//			String rowEnd = "</tr>";
//			String colBegin = "<td>";
//			String colEnd = "</td>";
//			this.output.write(head.getBytes());// 写如头部
//			ZipFile xlsxFile = new ZipFile(new File(this.filePath));
//			ZipEntry sharedStringXML = xlsxFile.getEntry("word/document.xml");
//			InputStream inputStream = xlsxFile.getInputStream(sharedStringXML);
//			XmlPullParser xmlParser = Xml.newPullParser();
//			xmlParser.setInput(inputStream, "utf-8");
//			int evtType = xmlParser.getEventType();
//			boolean isTable = false; // 是表格 用来统计 列 行 数
//			boolean isSize = false; // 大小状态
//			boolean isColor = false; // 颜色状态
//			boolean isCenter = false; // 居中状态
//			boolean isRight = false; // 居右状态
//			boolean isItalic = false; // 是斜体
//			boolean isUnderline = false; // 是下划线
//			boolean isBold = false; // 加粗
//			boolean isR = false; // 在那个r中
//			int pictureIndex = 1; // docx 压缩包中的图片名 iamge1 开始 所以索引从1开始
//			while (evtType != XmlPullParser.END_DOCUMENT) {
//				switch (evtType) {
//
//				// 开始标签
//				case XmlPullParser.START_TAG:
//					String tag = xmlParser.getName();
//					System.out.println(tag);
//
//					if (tag.equalsIgnoreCase("r")) {
//						isR = true;
//					}
//					if (tag.equalsIgnoreCase("u")) { // 判断下划线
//						isUnderline = true;
//					}
//					if (tag.equalsIgnoreCase("jc")) { // 判断对齐方式
//						String align = xmlParser.getAttributeValue(0);
//						if (align.equals("center")) {
//							this.output.write("<center>".getBytes());
//							isCenter = true;
//						}
//						if (align.equals("right")) {
//							this.output.write("<div align=\"right\">"
//									.getBytes());
//							isRight = true;
//						}
//					}
//					if (tag.equalsIgnoreCase("color")) { // 判断颜色
//						String color = xmlParser.getAttributeValue(0);
//						this.output.write(("<font color=" + color + ">")
//								.getBytes());
//						isColor = true;
//					}
//					if (tag.equalsIgnoreCase("sz")) { // 判断大小
//						if (isR == true) {
//							int size = decideSize(Integer.valueOf(xmlParser
//									.getAttributeValue(0)));
//							this.output.write(("<font size=" + size + ">")
//									.getBytes());
//							isSize = true;
//						}
//					}
//					// 下面是表格处理
//					if (tag.equalsIgnoreCase("tbl")) { // 检测到tbl 表格开始
//						this.output.write(tableBegin.getBytes());
//						isTable = true;
//					}
//					if (tag.equalsIgnoreCase("tr")) { // 行
//						this.output.write(rowBegin.getBytes());
//					}
//					if (tag.equalsIgnoreCase("tc")) { // 列
//						this.output.write(colBegin.getBytes());
//					}
//
//					if (tag.equalsIgnoreCase("pic")) { // 检测到标签 pic 图片
//						String entryName_jpeg = "word/media/image"
//								+ pictureIndex + ".jpeg";
//						String entryName_png = "word/media/image"
//								+ pictureIndex + ".png";
//						String entryName_gif = "word/media/image"
//								+ pictureIndex + ".gif";
//						ZipEntry sharePicture = null;
//						InputStream pictIS = null;
//						sharePicture = xlsxFile.getEntry(entryName_jpeg);
//						// 一下为读取docx的图片 转化为流数组
//						if (sharePicture == null) {
//							sharePicture = xlsxFile.getEntry(entryName_png);
//						}
//						if(sharePicture == null){
//							sharePicture = xlsxFile.getEntry(entryName_gif);
//						}
//						pictIS = xlsxFile.getInputStream(sharePicture);
//						ByteArrayOutputStream pOut = new ByteArrayOutputStream();
//						byte[] bt = null;
//						byte[] b = new byte[1000];
//						int len = 0;
//						while ((len = pictIS.read(b)) != -1) {
//							pOut.write(b, 0, len);
//						}
//						pictIS.close();
//						pOut.close();
//						bt = pOut.toByteArray();
//						Log.i("byteArray", "" + bt);
//						if (pictIS != null)
//							pictIS.close();
//						if (pOut != null)
//							pOut.close();
//						writeDOCXPicture(bt);
//						pictureIndex++; // 转换一张后 索引+1
//					}
//
//					if (tag.equalsIgnoreCase("b")) { // 检测到加粗标签
//						isBold = true;
//					}
//					if (tag.equalsIgnoreCase("p")) {// 检测到 p 标签
//						if (isTable == false) { // 如果在表格中 就无视
//							this.output.write(tagBegin.getBytes());
//						}
//					}
//					if (tag.equalsIgnoreCase("i")) { // 斜体
//						isItalic = true;
//					}
//					// 检测到值 标签
//					if (tag.equalsIgnoreCase("t")) {
//						if (isBold == true) { // 加粗
//							this.output.write("<b>".getBytes());
//						}
//						if (isUnderline == true) { // 检测到下划线标签,输入<u>
//							this.output.write("<u>".getBytes());
//						}
//						if (isItalic == true) { // 检测到斜体标签,输入<i>
//							output.write("<i>".getBytes());
//						}
//						river = xmlParser.nextText();
//						this.output.write(river.getBytes()); // 写入数值
//						if (isItalic == true) { // 检测到斜体标签,在输入值之后,输入</i>,并且斜体状态=false
//							this.output.write("</i>".getBytes());
//							isItalic = false;
//						}
//						if (isUnderline == true) {// 检测到下划线标签,在输入值之后,输入</u>,并且下划线状态=false
//							this.output.write("</u>".getBytes());
//							isUnderline = false;
//						}
//						if (isBold == true) { // 加粗
//							this.output.write("</b>".getBytes());
//							isBold = false;
//						}
//						if (isSize == true) { // 检测到大小设置,输入结束标签
//							this.output.write("</font>".getBytes());
//							isSize = false;
//						}
//						if (isColor == true) { // 检测到颜色设置存在,输入结束标签
//							this.output.write("</font>".getBytes());
//							isColor = false;
//						}
//						if (isCenter == true) { // 检测到居中,输入结束标签
//							this.output.write("</center>".getBytes());
//							isCenter = false;
//						}
//						if (isRight == true) { // 居右不能使用<right></right>,使用div可能会有状况,先用着
//							this.output.write("</div>".getBytes());
//							isRight = false;
//						}
//					}
//					break;
//				// 结束标签
//				case XmlPullParser.END_TAG:
//					String tag2 = xmlParser.getName();
//					if (tag2.equalsIgnoreCase("tbl")) { // 检测到表格结束,更改表格状态
//						this.output.write(tableEnd.getBytes());
//						isTable = false;
//					}
//					if (tag2.equalsIgnoreCase("tr")) { // 行结束
//						this.output.write(rowEnd.getBytes());
//					}
//					if (tag2.equalsIgnoreCase("tc")) { // 列结束
//						this.output.write(colEnd.getBytes());
//					}
//					if (tag2.equalsIgnoreCase("p")) { // p结束,如果在表格中就无视
//						if (isTable == false) {
//							this.output.write(tagEnd.getBytes());
//						}
//					}
//					if (tag2.equalsIgnoreCase("r")) {
//						isR = false;
//					}
//					break;
//				default:
//					break;
//				}
//				evtType = xmlParser.next();
//			}
//			this.output.write(end.getBytes());
//		} catch (ZipException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (XmlPullParserException e) {
//			e.printStackTrace();
//		}
//		if (river == null) {
//			river = "解析文件出现问题";
//		}
//	}
//
//
//	/**
//	 *
//	 * 2013-5-15 下午2:47:34 chenhao添加此方法
//	 * @throws Exception
//	 */
//	private void readXLS() throws Exception{
//		List<PicInfo> picInfos = new ArrayList<PicInfo>();
//		myFile = new File(htmlPath);
//		output = new FileOutputStream(myFile);
//		lsb.append("<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'>");
//		lsb.append("<head><meta http-equiv=Content-Type content='text/html; charset=utf-8'><meta name=ProgId content=Excel.Sheet>");
//		HSSFSheet sheet = null;
//
//		String excelFileName = filePath;
//		try {
//			FileInputStream fis  = new FileInputStream(excelFileName);
//			HSSFWorkbook workbook = (HSSFWorkbook) WorkbookFactory.create(fis); // 获整个Excel
//			for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
//				if (workbook.getSheetAt(sheetIndex) != null) {
//					sheet = workbook.getSheetAt(sheetIndex);// 获得不为空的这个sheet
//					if(picInfos.size() > 0 && picInfos != null){
//						picInfos.clear();
//					}
//					if(sheet.getDrawingPatriarch() != null){
//						List<HSSFShape>  shapes = sheet.getDrawingPatriarch().getChildren();
//						for(HSSFShape shape : shapes){
//							HSSFClientAnchor anchor = (HSSFClientAnchor) shape.getAnchor();
//							if(shape instanceof HSSFPicture){
//								HSSFPicture pic = (HSSFPicture) shape;
//								pic.getPictureData();
//								PicInfo info = new PicInfo();
//
//								int row = anchor.getRow1(); // 得到锚点的起始行号
//
//								int col = anchor.getCol1();// 得到锚点的起始列号
//
//								info.setColNum(col);
//								info.setRowNum(row);
//								info.setSheetName(sheet.getSheetName());
//								HSSFPictureData picData = pic.getPictureData();
//								try {
//									info = savePic(info, sheet.getSheetName(), picData);
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
//								picInfos.add(info);
//							}
//						}
//
//					}
//						int firstRowNum = sheet.getFirstRowNum(); // 第一行
//						int lastRowNum = sheet.getLastRowNum(); // 最后一行
//						// 构造Table
//						lsb.append("<table width=\"100%\" style=\"border:1px solid #000;border-width:1px 0 0 1px;margin:2px 0 2px 0;border-collapse:collapse;\">");
//						for (int rowNum = firstRowNum; rowNum <= lastRowNum; rowNum++) {
//							if (sheet.getRow(rowNum) != null) {// 如果行不为空，
//								HSSFRow row = sheet.getRow(rowNum);
//								short firstCellNum = row.getFirstCellNum(); // 该行的第一个单元格
//								short lastCellNum = row.getLastCellNum(); // 该行的最后一个单元格
//								int height = (int) (row.getHeight() / 15.625); // 行的高度
//								lsb.append("<tr height=\""
//										+ height
//										+ "\" style=\"border:1px solid #000;border-width:0 1px 1px 0;margin:2px 0 2px 0;\">");
//								for (short cellNum = firstCellNum; cellNum <= lastCellNum; cellNum++) { // 循环该行的每一个单元格
//									HSSFCell cell = row.getCell(cellNum);
//										StringBuffer tdStyle = new StringBuffer(
//												"<td style=\"border:1px solid #000; border-width:0 1px 1px 0;margin:2px 0 2px 0; ");
//										if(null != cell){
//										HSSFCellStyle cellStyle = cell
//												.getCellStyle();
//
//									    short boldWeight = cellStyle.getFont(
//												workbook).getBoldweight(); // 字体粗细
//										short fontHeight = (short) (cellStyle
//												.getFont(workbook)
//												.getFontHeight() / 2); // 字体大小
//										tdStyle.append(" font-weight:"
//												+ boldWeight + "; ");
//										tdStyle.append(" font-size: "
//												+ fontHeight + "%;");
//										lsb.append(tdStyle + "\"");
//
//
//										}
//										boolean flag = false;
//										if (picInfos.size() > 0
//												&& picInfos != null) {
//											for (PicInfo picInfo : picInfos) {
//												// 找到图片对应的单元格
//												if (picInfo.getSheetName().equals(sheet.getSheetName()) && picInfo.getRowNum() == rowNum &&  picInfo.getColNum() == cellNum
//														 ) {
//													flag = true;
//													String imagePath = "<img src=\""
//															+ picInfo.getPicPath()
//															+ "\""
//															+ "/>";
//													lsb.append("<td>");
//													lsb.append(">" + imagePath
//															+ "</td>");
//												}
//											}
//										}
//										if(null != cell && !flag){
//											lsb.append(">" + getCellValue(cell)
//													+ "</td>");
//										}
//
//								}
//								lsb.append("</tr>");
//							}
//						}
//
//
//				}
//
//			}
//			output.write(lsb.toString().getBytes());
//			fis.close();
//		} catch (FileNotFoundException e) {
//			throw new Exception("文件 " + excelFileName + " 没有找到!");
//		} catch (IOException e) {
//			throw new Exception("文件 " + excelFileName + " 处理错误("
//					+ e.getMessage() + ")!");
//		}
//
//	}
//
//	private void readXLSX() {
//		try {
//			this.myFile = new File(this.htmlPath);// new一个File,路径为html文件
//			this.output = new FileOutputStream(this.myFile);// new一个流,目标为html文件
//			String head = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\"http://www.w3.org/TR/html4/loose.dtd\"><html><meta charset=\"utf-8\"><head></head><body>";// 定义头文件,我在这里加了utf-8,不然会出现乱码
//			String tableBegin = "<table style=\"border-collapse:collapse\" border=1 bordercolor=\"black\">";
//			String tableEnd = "</table>";
//			String rowBegin = "<tr>";
//			String rowEnd = "</tr>";
//			String colBegin = "<td>";
//			String colEnd = "</td>";
//			String end = "</body></html>";
//			this.output.write(head.getBytes());
//			this.output.write(tableBegin.getBytes());
//			String str = "";
//			String v = null;
//			boolean flat = false;
//			List<String> ls = new ArrayList<String>();
//			try {
//				ZipFile xlsxFile = new ZipFile(new File(this.filePath));// 地址
//				ZipEntry sharedStringXML = xlsxFile
//						.getEntry("xl/sharedStrings.xml");// 共享字符串
//				InputStream inputStream = xlsxFile
//						.getInputStream(sharedStringXML);// 输入流 目标上面的共享字符串
//				XmlPullParser xmlParser = Xml.newPullParser();// new 解析器
//				xmlParser.setInput(inputStream, "utf-8");// 设置解析器类型
//				int evtType = xmlParser.getEventType();// 获取解析器的事件类型
//				while (evtType != XmlPullParser.END_DOCUMENT) {// 如果不等于 文档结束
//					switch (evtType) {
//					case XmlPullParser.START_TAG: // 标签开始
//						String tag = xmlParser.getName();
//						if (tag.equalsIgnoreCase("t")) {
//							ls.add(xmlParser.nextText());
//						}
//						break;
//					case XmlPullParser.END_TAG: // 标签结束
//						break;
//					default:
//						break;
//					}
//					evtType = xmlParser.next();
//				}
//				ZipEntry sheetXML = xlsxFile
//						.getEntry("xl/worksheets/sheet1.xml");
//				InputStream inputStreamsheet = xlsxFile
//						.getInputStream(sheetXML);
//				XmlPullParser xmlParsersheet = Xml.newPullParser();
//				xmlParsersheet.setInput(inputStreamsheet, "utf-8");
//				int evtTypesheet = xmlParsersheet.getEventType();
//				this.output.write(rowBegin.getBytes());
//				int i = -1;
//				while (evtTypesheet != XmlPullParser.END_DOCUMENT) {
//					switch (evtTypesheet) {
//					case XmlPullParser.START_TAG: // 标签开始
//						String tag = xmlParsersheet.getName();
//						if (tag.equalsIgnoreCase("row")) {
//						} else {
//							if (tag.equalsIgnoreCase("c")) {
//								String t = xmlParsersheet.getAttributeValue(
//										null, "t");
//								if (t != null) {
//									flat = true;
//									System.out.println(flat + "有");
//								} else {// 没有数据时 找了我n年,终于找到了 输入<td></td> 表示空格
//									this.output.write(colBegin.getBytes());
//									this.output.write(colEnd.getBytes());
//									System.out.println(flat + "没有");
//									flat = false;
//								}
//							} else {
//								if (tag.equalsIgnoreCase("v")) {
//									v = xmlParsersheet.nextText();
//									this.output.write(colBegin.getBytes());
//									if (v != null) {
//										if (flat) {
//											str = ls.get(Integer.parseInt(v));
//										} else {
//											str = v;
//										}
//										this.output.write(str.getBytes());
//										this.output.write(colEnd.getBytes());
//									}
//								}
//							}
//						}
//						break;
//					case XmlPullParser.END_TAG:
//						if (xmlParsersheet.getName().equalsIgnoreCase("row")
//								&& v != null) {
//							if (i == 1) {
//								this.output.write(rowEnd.getBytes());
//								this.output.write(rowBegin.getBytes());
//								i = 1;
//							} else {
//								this.output.write(rowBegin.getBytes());
//							}
//						}
//						break;
//					}
//					evtTypesheet = xmlParsersheet.next();
//				}
//				System.out.println(str);
//			} catch (ZipException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			} catch (XmlPullParserException e) {
//				e.printStackTrace();
//			}
//			if (str == null) {
//				str = "解析文件出现问题";
//			}
//			this.output.write(rowEnd.getBytes());
//			this.output.write(tableEnd.getBytes());
//			this.output.write(end.getBytes());
//		} catch (Exception e) {
//			System.out.println("readAndWrite Exception");
//		}
//	}
//
//	/**
//	 * 保存excel中的图片
//	 * 2013-5-16 下午5:20:54 chenhao添加此方法
//	 * @param info
//	 * @param sheetName
//	 * @param picData
//	 * @return
//	 */
//	private PicInfo savePic(PicInfo info, String sheetName,
//			HSSFPictureData picData) {
//		if (Environment.getExternalStorageState().equals(
//				Environment.MEDIA_MOUNTED)) {	// 判断sdcard是否挂载正常
//			byte[] data = picData.getData();
//			File picFile = new File(
//					Environment.getExternalStorageDirectory()//"webgisams"+File.separator+"attrs"+File.separator+"pics";
//							+File.separator+ "webgisams" +File.separator+ "attrs"+File.separator+sheetName + "_" + info.getRowNum()+"_"+info.getColNum() + ".jpg");
//			if (!picFile.exists()) {
//				picFile.getParentFile().mkdirs();
//			}
//			info.setPicName(picFile.getName());
//			info.setPicPath(picFile.getAbsolutePath());
//			try {
//				FileOutputStream fos = new FileOutputStream(picFile);
//				fos.write(data);
//				fos.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//		} else {
//			throw new RuntimeException("sd卡挂载异常！");
//		}
//
//		return info;
//
//	}
//
//
//	/**
//	 * 取得单元格的值
//	 *
//	 * @param cell
//	 * @return
//	 * @throws java.io.IOException
//	 */
//	private  Object getCellValue(HSSFCell cell) throws IOException {
//		Object value = "";
//		if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
//			value = cell.getRichStringCellValue().toString();
//		} else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//			if (HSSFDateUtil.isCellDateFormatted(cell)) {
//				Date date = (Date) cell.getDateCellValue();
//				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//				value = sdf.format(date);
//			} else {
//				double value_temp = (double) cell.getNumericCellValue();
//				BigDecimal bd = new BigDecimal(value_temp);
//				BigDecimal bd1 = bd.setScale(3, bd.ROUND_HALF_UP);
//				value = bd1.doubleValue();
//				DecimalFormat format = new DecimalFormat("#0.###");
//				value = format.format(cell.getNumericCellValue());
//
//			}
//		}
//
//		if (cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
//			value = "";
//		}
//		return value;
//	}
//
//
//
//	/**
//	 * 读取word文档中的图片
//	 * 2013-5-15 下午2:41:57 chenhao添加此方法
//	 * @param pictureBytes
//	 */
//	private void writeDOCXPicture(byte[] pictureBytes) {
//		makePictureFile();
//		this.presentPicture++;
//		File myPicture = new File(this.picturePath);
//		try {
//			FileOutputStream outputPicture = new FileOutputStream(myPicture);
//			outputPicture.write(pictureBytes);
//			outputPicture.close();
//		} catch (Exception e) {
//			System.out.println("outputPicture Exception");
//		}
//		String imageString = "<img src=\"" + this.picturePath + "\"";
//		imageString = imageString + "/>";
//		try {
//			this.output.write(imageString.getBytes());
//		} catch (Exception e) {
//			System.out.println("output Exception");
//		}
//	}
//
//	/**
//	 * 读写文档中的段落
//	 * 2013-5-15 上午11:11:11 chenhao添加此方法
//	 * @param paragraph
//	 * @throws Exception
//	 */
//	private void writeParagraphContent(Paragraph paragraph) throws Exception {
//		Paragraph p = paragraph;
//		int pnumCharacterRuns = p.numCharacterRuns();
//
//		for (int j = 0; j < pnumCharacterRuns; j++) {
//
//			CharacterRun run = p.getCharacterRun(j);
//			if (run.getPicOffset() == 0 || run.getPicOffset() >= 1000) {
//				if (presentPicture < pictures.size()) {
//					writePicture();
//				}
//			} else {
//				try {
//					String text = run.text();
//					if (text.length() >= 2 && pnumCharacterRuns < 2) {
//						output.write(text.getBytes());
//					} else {
//						int size = run.getFontSize();
//						int color = run.getColor();
//						String fontSizeBegin = "<font size=\""
//								+ decideSize(size) + "\">";
//						String fontColorBegin = "<font color=\""
//								+ decideColor(color) + "\">";
//						String fontEnd = "</font>";
//						String boldBegin = "<b>";
//						String boldEnd = "</b>";
//						String islaBegin = "<i>";
//						String islaEnd = "</i>";
//
//						output.write(fontSizeBegin.getBytes());
//						output.write(fontColorBegin.getBytes());
//
//						if (run.isBold()) {
//							output.write(boldBegin.getBytes());
//						}
//						if (run.isItalic()) {
//							output.write(islaBegin.getBytes());
//						}
//
//						output.write(text.getBytes());
//
//						if (run.isBold()) {
//							output.write(boldEnd.getBytes());
//						}
//						if (run.isItalic()) {
//							output.write(islaEnd.getBytes());
//						}
//						output.write(fontEnd.getBytes());
//						output.write(fontEnd.getBytes());
//					}
//				} catch (Exception e) {
//					System.out.println("Write File Exception");
//				}
//			}
//		}
//	}
//
//	/**
//	 * 读取文档中的图片
//	 * 2013-5-15 上午11:13:57 chenhao添加此方法
//	 */
//	private void writePicture() {
//		Picture picture = (Picture) pictures.get(presentPicture);
//
//		byte[] pictureBytes = picture.getContent();
//
//		makePictureFile();
//		presentPicture++;
//
//		File myPicture = new File(picturePath);
//
//		try {
//
//			FileOutputStream outputPicture = new FileOutputStream(myPicture);
//
//			outputPicture.write(pictureBytes);
//
//			outputPicture.close();
//		} catch (Exception e) {
//			System.out.println("outputPicture Exception");
//		}
//
//		String imageString = "<img src=\"" + picturePath + "\"";
//
//		imageString = imageString + "/>";
//
//		try {
//			output.write(imageString.getBytes());
//		} catch (Exception e) {
//			System.out.println("output Exception");
//		}
//	}
//
//	/**
//	 *  用来在sdcard上创建图片
//	 */
//	public void makePictureFile() {
//		String sdString = Environment.getExternalStorageState();// 获取外部存储状态
//		if (sdString.equals(Environment.MEDIA_MOUNTED)) {// 确认sd卡存在,原理不知
//			try {
//				File picFile = Environment
//						.getExternalStorageDirectory();// 获取sd卡目录
//				String picPath = picFile.getAbsolutePath() + File.separator
//						+ "webgisams"+File.separator+"attrs"+File.separator+"pics";// 创建目录,上面有解释
//				File picDirFile = new File(picPath);
//				if (!picDirFile.exists()) {
//					picDirFile.mkdir();
//				}
//				File pictureFile = new File(picPath + File.separator
//						+ presentPicture + ".jpg");// 创建jpg文件,方法与html相同
//				if (!pictureFile.exists()) {
//					pictureFile.createNewFile();
//				}
//				this.picturePath = pictureFile.getAbsolutePath();// 获取jpg文件绝对路径
//			} catch (Exception e) {
//				System.out.println("PictureFile Catch Exception");
//			}
//		}
//	}
//
//	private int decideSize(int size) {
//
//		if (size >= 1 && size <= 8) {
//			return 1;
//		}
//		if (size >= 9 && size <= 11) {
//			return 2;
//		}
//		if (size >= 12 && size <= 14) {
//			return 3;
//		}
//		if (size >= 15 && size <= 19) {
//			return 4;
//		}
//		if (size >= 20 && size <= 29) {
//			return 5;
//		}
//		if (size >= 30 && size <= 39) {
//			return 6;
//		}
//		if (size >= 40) {
//			return 7;
//		}
//		return 3;
//	}
//
//	private String decideColor(int a) {
//		int color = a;
//		switch (color) {
//		case 1:
//			return "#000000";
//		case 2:
//			return "#0000FF";
//		case 3:
//		case 4:
//			return "#00FF00";
//		case 5:
//		case 6:
//			return "#FF0000";
//		case 7:
//			return "#FFFF00";
//		case 8:
//			return "#FFFFFF";
//		case 9:
//			return "#CCCCCC";
//		case 10:
//		case 11:
//			return "#00FF00";
//		case 12:
//			return "#080808";
//		case 13:
//		case 14:
//			return "#FFFF00";
//		case 15:
//			return "#CCCCCC";
//		case 16:
//			return "#080808";
//		default:
//			return "#000000";
//		}
//	}
//
//}
