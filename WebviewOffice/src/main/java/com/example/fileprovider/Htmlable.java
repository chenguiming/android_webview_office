package com.example.fileprovider;
import java.io.InputStream;
/**
 * project:
 * author: wzq
 * date: 2014/7/2
 * description:
 */
public interface Htmlable {

    // 进行转换
    boolean convertToHtml();

    // 获取转换结果
    InputStream getHtmlInputStream();
}
