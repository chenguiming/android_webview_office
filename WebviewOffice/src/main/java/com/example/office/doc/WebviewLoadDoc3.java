package com.example.office.doc;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.example.fileprovider.FileProvider;
import com.example.office.WebViewActivity;

/**
 * 读取 doc文件
 * 使用content provider方式
 */
public class WebviewLoadDoc3 extends WebViewActivity {

    // 测试发现 .doc后缀名 不能正常显示  .doc => _doc.html
    // 估计是mimeType的问题
    // private String file = SD_ROOT + "/_test/doc_en.html"; // e文 ok
    // private String file = SD_ROOT + "/_test/doc_cn.html"; // 中文 ok
    // private String file = SD_ROOT + "/_test/doc_pic.html"; // 图片 ok
    private String file = SD_ROOT + "/_test/doc_pic.html"; // 图文 ok
    // private String file = SD_ROOT + "/_test/pic_txt.doc"; // 图片 文字 使用doc后缀名 fail
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("file = " + file);
        mCtx = this;

        //
        init();
    }

    private void init() {
        btnLoad.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "content://" + FileProvider.AUTH + file;
                System.out.println("url = " + url);
                webview.loadUrl(url);
            }
        });
    }
}
