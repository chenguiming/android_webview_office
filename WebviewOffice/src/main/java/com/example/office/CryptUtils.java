package com.example.office;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * byte[] Stream String File 相互转换用到的工具类
 *
 * @author wzq
 * @2014年5月5日
 */
public class CryptUtils {
    private static final String BYTE_FORMAT = "%02x";

    /**
     * 打印一个byte[]
     */
    public static String printByteArray(byte[] bytes) {
        StringBuilder sb = new StringBuilder(1024);
        for (byte b : bytes) {
            sb.append(printByte(b)) //
                    .append(" ");
        }
        return sb.toString();
    }

    /**
     * 打印一个byte,如 0xab 的打印结果 => ab
     */
    public static String printByte(byte b) {
        return String.format(BYTE_FORMAT, b);
    }

    /**
     * InputStream => String
     *
     * @param in
     * @param encoding null= "UTF-8"
     * @return
     * @throws java.io.IOException
     */
    public static String stream2String(InputStream in, String encoding) throws IOException {
        // 效率更高
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, encoding));
        StringBuilder sb = new StringBuilder();
        final int BUFFER_SIZE = 1024;
        char[] buffer = new char[BUFFER_SIZE];
        int nRead = 0;
        while ((nRead = reader.read(buffer)) != -1) {
            sb.append(buffer, 0, nRead);
        }
        return sb.toString();
    }

    /**
     * inputStream => byte[]
     *
     * @param is
     * @return
     * @throws java.io.IOException
     */
    public static byte[] stream2Bytes(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        final int BUFFER_SIZE = 1024;
        byte[] data = new byte[BUFFER_SIZE];
        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        byte[] bytes = buffer.toByteArray();
        return bytes;
    }

    /**
     * is => file
     *
     * @param in
     * @param fullPath 存储的路径名称
     * @throws java.io.IOException
     */
    public static void stream2File(InputStream in, String fullPath) throws IOException {
        File file = new File(fullPath);
        stream2File(in, file);
    }

    /**
     * is => file
     *
     * @param in
     * @param file
     * @throws java.io.IOException
     */
    public static void stream2File(InputStream in, File file) throws IOException {
        // mkdirs
        File dir = file.getParentFile();
        if (dir != null && !dir.exists()) {
            dir.mkdirs();
        }

        //
        OutputStream os = new FileOutputStream(file);
        int bytesRead = 0;
        final int BUFFER_SIZE = 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        while ((bytesRead = in.read(buffer, 0, buffer.length)) != -1) {
            os.write(buffer, 0, bytesRead);
        }
        os.flush();
        os.close();
        in.close();
    }

    /**
     * byte[] => file
     *
     * @param bytes
     * @param file 存储的路径名称
     * @throws java.io.IOException
     */
    public static void bytes2File(byte[] bytes, File file) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        bos.write(bytes);
        bos.flush();
        bos.close();
    }

    /**
     * file => byte[]
     *
     * @param file
     * @throws java.io.IOException
     */
    public static byte[] file2Bytes(File file) throws IOException {
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
        buf.read(bytes, 0, bytes.length);
        buf.close();
        return bytes;
    }

    /**
     * byte[] => InputStream
     *
     * @param in
     * @throws Exception
     */
    public static InputStream bytes2Stream(byte[] in) throws IOException {
        ByteArrayInputStream is = new ByteArrayInputStream(in);
        return is;
    }

    /**
     * byte[] => String
     *
     * @param bytes
     * @param encoding null=utf-8
     * @throws java.io.IOException
     */
    public static String bytes2String(byte[] bytes, String encoding) throws IOException {
        InputStream is = bytes2Stream(bytes);
        if (encoding == null) {
            encoding = "UTF-8";
        }
        return stream2String(is, encoding);
    }

    /**
     * String=> byte[]
     *
     * @param str
     * @param encoding null=utf-8
     * @throws java.io.IOException
     */
    public static byte[] string2Bytes(String str, String encoding) throws IOException {
        if (encoding == null) {
            encoding = "UTF-8";
        }
        return str.getBytes(encoding);
    }
}
