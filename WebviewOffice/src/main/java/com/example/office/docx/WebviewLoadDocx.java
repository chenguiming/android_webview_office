package com.example.office.docx;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.example.fileprovider.FileProvider;
import com.example.office.WebViewActivity;
/**
 * @author: wzq
 * @date: 14-6-26
 * description: change it at File | setting | File and code templates | include | file header
 */
public class WebviewLoadDocx extends WebViewActivity {
    // private String file = SD_ROOT + "/_test/docx_en.html"; // e文 ok
    // private String file = SD_ROOT + "/_test/docx_cn.html"; // 中文 ok
    //private String file = SD_ROOT + "/_test/docx_pic.html"; // 图片 ok
    private String file = SD_ROOT + "/_test/docx_sample.html"; // 重复图片 ok

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("file = " + file);
        mCtx = this;

        //
        init();
    }

    private void init() {
        btnLoad.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "content://" + FileProvider.AUTH + file;
                System.out.println("url = " + url);
                webview.loadUrl(url);
            }
        });
    }
}
