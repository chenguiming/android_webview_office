package com.example.office.xls;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.example.fileprovider.FileProvider;
import com.example.office.WebViewActivity;
/**
 * project:
 * author: wzq
 * date: 2014/7/2
 * description:
 */
public class WebviewLoadXls extends WebViewActivity {
    private String file = SD_ROOT + "/_test/xls_pic.html"; // e文

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("file = " + file);
        mCtx = this;

        //
        init();
    }

    private void init() {
        btnLoad.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "content://" + FileProvider.AUTH + file;
                System.out.println("url = " + url);
                webview.loadUrl(url);
            }
        });
    }
}
